//
//  ViewController.swift
//  20160512_iOSClassDay11_Pan
//
//  Created by ChenSean on 5/12/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var orgPoint = CGPointZero
    
    @IBAction func handlePanGesture(sender: UIPanGestureRecognizer) {
        
        
        for i in 0 ..< sender.numberOfTouches() {
            var point = sender.locationOfTouch(i, inView: self.view)
            
            print("x = \(point.x), y = \(point.y)")
            
            if sender.state == .Began{
                orgPoint = CGPointMake(point.x - (sender.view?.center.x)!, point.y - (sender.view?.center.y)!)
                
            } else if sender.state == .Changed{
                
                point = CGPointMake(point.x - orgPoint.x, point.y - orgPoint.y)
                sender.view?.center = point
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

